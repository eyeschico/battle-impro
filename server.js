// Express
const express = require('express');
const app = express();
const port = process.env.PORT || 8080;
// Body-parser
const bodyParser = require('body-parser');
// SQLite
const sqlite3 = require('sqlite3').verbose();
// Set the view engine to EJS
app.set('view engine', 'ejs');
// For parsing application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true })); 
// Connect to static file like css
app.use(express.static(__dirname + '/public'));

// Open database
let db = new sqlite3.Database('./db/battle_impro.db', sqlite3.OPEN_READWRITE, (err) => {
    if (err)
    {
        return console.error(err.message);
    }
    console.log('Connected to the Battle Impro SQlite database.');
});

//Listen for connections
app.listen(port, () => {
    console.log(`App running on http://localhost:${port}`)
});

///DATA///
let formData = {
    id: null,
    nom_sujet: ''
};

let formUserData = {
    id: null,
    name: '',
    email: '',
    password: '',
};


///ROUTES///

//Home page

//Récupérer tout les sujets
app.get('/home', (req, res) => {
    const query = `SELECT * FROM sujets`;
    db.all(query, [], (err, rows) => {
        if (err)
        {
            console.error(err.message);
        }

        res.render('pages/home', {
            sujets: rows
        });
    });
});

//Roulette page

//Roulette, récupère tout les sujets en me donnant qu'un seul sujet 
app.get('/roulette', (req, res) => {
    const query = `SELECT * FROM sujets ORDER BY RANDOM() LIMIT 1`;
    db.all(query, [], (err, rows) => {
        if (err)
        {
            console.error(err.message);
        }

        res.render('pages/roulette', {
            sujets: rows
        });
    });
});

//Index Page
app.get('/', (req, res) => {
    res.render('pages/index', {});
});

//Partial Register
app.get('/register', (req, res) => {
    res.render('pages/register', {});
});

//Partial Login
app.get('/', (req, res) => {
        res.render('partials/login', {});
});


///CRUD///

//Créer un sujet
app.post('/home', (req, res) => {
    const query = `INSERT INTO sujets (nom_sujet)
        VALUES ('${req.body.inputNomSujet}')`;

    db.run(query, (err) => {
        if (err)
        {
            return console.error('Erreur : ', err.message);
        }
        console.log('Sujet crée avec succès !');

        renderAddSujet(res);
    });
});

// Créer un utilisateur
app.post('/register', (req, res) => {
    const query = `INSERT INTO membres (name, email, password)
        VALUES ('${req.body.inputName}',
                '${req.body.inputEmail}',
                '${req.body.inputPassword}'
        )`;

    db.run(query, (err) => {
        if (err)
        {
            return console.error('Erreur : ', err.message);
        }
        console.log('Utilisateur crée avec succès !');

        renderAddUser(res);
    });
});

///UTILITIES///

function renderAddSujet(res) {
    resetFormData();

    const query = `SELECT * FROM sujets`;
    db.all(query, [], (err, rows) => {
        if (err)
        {
            console.error(' Erreur : ', err.message);
        }

        res.render('pages/home', {
            sujets: rows,
            formData: formData
        });
    });
}

function renderAddUser(res) {
    resetFormData();

    const query = `SELECT * FROM membres`;
    db.all(query, [], (err, rows) => {
        if (err)
        {
            console.error(' Erreur : ', err.message);
        }

        res.render('pages/register', {
            membres: rows,
            formUserData: formUserData
        });
    });
}

function resetFormData() {
    formData = {
        id: null,
        nom_sujet: ''
    };
}

function resetFormUserData() {
    formUserData = {
        id: null,
        name: '',
        email: '',
        password: '',
    };
}


