## Battle D'improvisation

#### Application qui permet dans un premier temps de saisir des sujets d'improvisation, puis d'en tirer au sort, aléatoirement.

##### Trello 

- https://trello.com/b/5U5MEjX4/battle-impro

##### Diagrammes

- Disponible a la racine du repo

##### Maquette

- https://www.figma.com/file/5JEd0ZmcYC7uZM8lFKc0xi/Maquette-Battle-impro?node-id=0%3A1

##### Heroku App

###### Page d'accueil
- https://battle-impro.herokuapp.com/

###### Page d'inscription
- https://battle-impro.herokuapp.com/register

###### Home page
- https://battle-impro.herokuapp.com/home

###### Page roulette
- https://battle-impro.herokuapp.com/roulette